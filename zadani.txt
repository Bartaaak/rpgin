Tato semestrální práce patří do kategorie her. Vaším cílem je vytvořit engine pro hru, nikoli konkrétní hru (příběh). Engine bude umožňovat definovat hru (příběh) prostřednictvím konfiguračních souborů. Nesnažte se definovat všechny části v kódu (a to ani pomocí konstant). Umístěte je do konfiguračních souborů (jednoho nebo více), které váš program bude načítat.

Vaším cílem je naprogramuovat engine pro textové RPG (gamebook), nesnažte se přímo ve vašem kódu implementovat třídy pro konkrétní příběh. Ten je definován v konfiguračních souborech a mělo by tak být jednoduché změnit "souboj s nejvyšším elfem" za "posezení na kávě s vesmírným slimákem". Textové RPG funguje na principu jednotlivých dialogů, kdy vám je aktuální prostředí textově popsáno (takzvaná místnost) a následně nabídnuto několik akcí, které se dají provést, nebo přejít do nějaké "sousední místnosti".

Váš engine musí implementovat následující funkcionality:

Umožněte vytvořit herní postavu, kde si uživatel může

navolit alespoň 3 různé druhy atributů (př. síla, obratnost, inteligence, …),

vybrat schopnost/i nebo povolání (př. válečník, lučištník, mág, …).

Každá schopnost (povolání) může mít více efektů, které mohou být aktivní (uživatel je musí použít) nebo pasivní (jsou aktivovány automaticky).

Interakce se světem probíhá pomocí textového popisu jednotlivých místností a zadáváním akcí prostřednictvím klávesnice (př. volba akce z předem definované nabídky, klíčová slova ala Zork nebo Albion, …).

Implementujte jednoduchý soubojový systém a inventář

Hra musí být konfigurovatelná ze souboru:

definice atributů (př. název, …) a schopností/povolání (př. název, cena, efekty, …)

definice světa (př. místnosti, předměty, příšery, dialogy, cíl, …)

Hra musí umožnit uložit a následně načíst rozehranou hru.
