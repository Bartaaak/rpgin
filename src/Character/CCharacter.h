#pragma once
#include <string>
#include <iostream>
#include <fstream>
/**
 *  The class represents a game character, that is playable (CHero) or unplayable that a hero fights (CENemy), both of these classes inherits from
 * CCharacter.
 */
class CCharacter {
public:
    /**
     *  The default constructor of CCharacter class
     */
    CCharacter()=default;

    /**
     * Virtual destructor that will destroy an object so that there are not any memory leaks.
     */
    virtual ~CCharacter()=default;


    friend class CWorldPrepare;
    /**
    *  This method will create a character based on configuration file. It can be either an enemy or a hero. It doesnt care, its virtual.
    * @param[in] file represents an input from configuration file, from which the character will be configurated.
    * @param[in] line_cnt represents number of lines in the configuration file
    */
    virtual void create_char(std::fstream & file, size_t & line_cnt )=0;
    /**
     *  This method will print character on a standart output
     */
    virtual void print_char()const=0 ;

protected:
    /**  Represents name of a character */
    std::string m_name;
    /**  Represents health of a character */
    int m_health;
    /**  Represents damage of a character*/
    int m_dmg;
    /**  Represents shield of a character*/
    int m_shield;




};


