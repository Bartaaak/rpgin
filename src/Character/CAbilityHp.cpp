

#include "CAbilityHp.h"

void CAbilityHp::import_pass_ability([[maybe_unused]]double& hp, [[maybe_unused]]double& dmg, [[maybe_unused]] double& sh)
{
    hp*=m_stat;
}
void CAbilityHp::insert_abilities(std::vector<std::unique_ptr<CAbility>> &entities)
{
    entities.emplace_back(std::make_unique<CAbilityHp>(*this));
}

void CAbilityHp::use_ability(CHero &player, [[maybe_unused]]CEnemy &enemy)
{
    player.m_real_health+=m_stat;

}

void CAbilityHp::print_ability() const {
    std::cout<<get_name()<<" "<<"| Health:"<<" "<<get_stat()<<std::endl;

}
