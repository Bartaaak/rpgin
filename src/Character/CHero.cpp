#include "CHero.h"

void CHero::create_char(std::fstream &file, size_t &line_cnt) //loads from a file
{
    std::string line;
    size_t cnt=0;
    while(std::getline(file,line))
    {
        line_cnt++;
        if(cnt==0) //loads name of a hero
        {
            if(change_format(line)=="")
            {
                std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
                throw std::runtime_error("The hero has no name.");
            }
            m_name= line; //deletes spaces that are not neccesery

        }
        if(cnt==1) { //load description of a hero
            m_description=line;
        }
        if(cnt==2) //loads health of a hero
        {
            int tmp= check_int_hero(line_cnt,line);
            if(tmp<=0)
            {
                std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
                throw std::runtime_error("The hero hp stat is not set correctly");
            }
            m_health=tmp;
        }
        if(cnt==3) { //loads a damage of a hero
            m_dmg= check_int_hero(line_cnt,line);
        }
        if(cnt==4) { //loads an shield of a hero
            int tmp= check_int_hero(line_cnt,line);
            if(tmp<=0)
            {
                std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
                throw std::runtime_error("The hero int stat is not set correctly");
            }
            m_shield= tmp;
        }

        if(cnt==5) //loads abilities of a hero
        {
            if(!read_abilities(line))
            {

                std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
                throw std::runtime_error("The ability id is not set correctly.");

            }
            break;
        }
        cnt++;
    }
    if(cnt<5) //if there are not enough arguments
    {
        std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
        throw std::runtime_error("The hero doesn't have enough arguments");
    }
}

int CHero::check_int_hero(size_t line_cnt, std::string &line)const //check whether a number in a string is allright
{
    int tmp=0;
    if(!check_int(line,tmp))
    {
        std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
        throw std::runtime_error("The hero stat is not set correctly");
    }
    return tmp;
}

void CHero::print_char() const
{
    std::cout << "Hero: " << m_name << std::endl << "           " << m_description << std::endl
              << "           " << "HP: " << m_health << "        " << "DMG: " << m_dmg << "        " << "SHIELD: " << m_shield << std::endl;

    }
bool CHero::read_abilities(std::string line) //insert abilities of a hero
{
    std::istringstream iss1(line);
    std::string tmp1;
    int tmp2=0;
    while (iss1 >> tmp1) {
        std::istringstream iss2(tmp1);
        if(!(iss2>>tmp2))
            return false;
        auto it=m_abilities.find(tmp2);
        if(it!=m_abilities.end())
            return false;
        m_abilities.insert(tmp2);
    }
    return true;

}



CHero &CHero::operator=(const CHero &other)
{
    if (this == &other)
        return *this;

    m_dmg=other.m_dmg;
    m_description=other.m_description;
    m_name=other.m_name;
    m_health=other.m_health;
    m_shield=other.m_shield;
    m_real_health=other.m_real_health;
    m_real_shield=other.m_real_shield;
    m_real_dmg=other.m_real_dmg;
    return *this;


}
