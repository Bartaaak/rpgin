
#include "CEnemy.h"

void CEnemy::create_char(std::fstream &file, size_t &line_cnt)//loads from a file
{
    std::string line;
    size_t cnt=0;
    while(std::getline(file,line))
    {
        line_cnt++;
        if(cnt==0) //loads a name
        {
            if(change_format(line)=="")
            {
                std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
                throw std::runtime_error("The enemy has no name.");
            }
            m_name=line; //deletes spaces that are not neccesery

        }

        if(cnt==1) //loads health points.
        {
            int tmp= check_int_enemy(line_cnt,line);
            if(tmp<=0)
            {
                std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
                throw std::runtime_error("The enemy hp stat is not set correctly");
            }
            m_health=tmp;
        }
        if(cnt==2) { //loads up damage of an enemy
            m_dmg= check_int_enemy(line_cnt,line);
        }
        if(cnt==3) { //loads up shield of an enemy
            m_shield= check_int_enemy(line_cnt, line);
            if(m_shield<0) //if an shield is lesser then 0
            {
                std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
                throw std::runtime_error("Config:The shield of an enemy is not set correctly.");
            }
            break;
        }

        cnt++;
    }
    if(cnt<3)
    {
        std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
        throw std::runtime_error("The enemy doesn't have enough arguments");
    }

}

void CEnemy::print_char() const
{
    std::cout << "Enemy: " << m_name << std::endl << "           "  << std::endl
              << "           " << "HP: " << m_health << "        " << "DMG: " << m_dmg << "        " << "SHIELD: " << m_shield << std::endl;

}

int CEnemy::check_int_enemy(size_t line_cnt, std::string &line)const
{
    int tmp=0;
    if(!check_int(line,tmp))
    {
        std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
        throw std::runtime_error("The enemy stat is not set correctly");
    }
    return tmp;
}