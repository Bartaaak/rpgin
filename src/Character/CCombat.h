#pragma once
#include "../Config/CWorldPlay.h"
#include "../Character/CEnemy.h"
#include <vector>
/**
 * Represents combat system in a game
 */
class CCombat
{
public:
    /**
     * Represents a fight between a player and an enemy
     * @param[in] player user
     * @param[in] enemy
     * @return true if a player won
     */
    bool fight(CWorldPlay & player, CEnemy & enemy);
    /**
     * prints out hero
     * @param[in] hero
     */
    void print_friend(const CHero& hero)const ;
    /**
     * prints out enemy
     * @param[in] enemy
     */
    void print_enemy(const CEnemy& enemy)const ;

    /**
     * prints everything
     * @param[in] player
     * @param[in] enemy
     */
    void print_everything(const CWorldPlay & player,const CEnemy& enemy)const;

    /**
     * If a hero is harmed
     * @param[in] hero user
     * @param[in] enemy
     */
    void take_damage_hero(CHero &hero,CEnemy &enemy);
    /**
     * if an enemy is harmed
     * @param[in] enemy
     * @param[in] hero
     */
    void take_damage_enemy(CEnemy &enemy,CHero &hero);
protected:
};


