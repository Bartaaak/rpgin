#pragma once

#include <sstream>
#include <algorithm>
#include "CCharacter.h"
#include "../utils.h"
/**
 * This class represents an unplayable enemy, that hero fights
 */
class CEnemy: public CCharacter
{
public:
    /**
     *  Default constructor
     */
    CEnemy()=default;
    /**
     *  Getter for a name of an enemy
     * @return name of an enemy
     */
    std::string get_name() const {
        return m_name;
    }
    /**
     * Getter for health of an enemy
     * @return health
     */
    int get_health()const{
        return m_health;
    }
    /**
     * Declare CCombat as friend class so that it can access the protected members
     */
    friend class CCombat;
    friend class CAbilityDmg;

    /**
     *  The method will create an enemy based on configuration file
     * @param[in] file fstream that represents the file
     * @param[in] line_cnt number of lines of a configuration file so far, so that error message can show where the mistake is.
     */
    void create_char(std::fstream &file, size_t &line_cnt) override;
    /**
     *  The method will check if a string contains a valid number, if yes it returns the number, otherwise it throws and exception
     * @param[in] line_cnt number of lines of a configuration file so far
     * @param[in] line     string to be checked
     * @return         number that is in a string.
     */
    int check_int_enemy( size_t line_cnt,std::string & line)const;
    /**
     *  Prints an enemy on a standart input.
     */
    void print_char ()const override ;


};




