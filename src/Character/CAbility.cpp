
#include "CAbility.h"
#include "../utils.h"
#include <iostream>
#include <fstream>
#include <string>
#include <ostream>
#define PASSIVE_ABILITY "P" // Sets ability to be passive
#define ACTIVE_ABILITY "A" // Sets ability to be active

void CAbility::create_ability(std::fstream &file,size_t &line_cnt)
{
    std::string line;
    size_t cnt=0;

    while(std::getline(file,line)) //loads from a file
    {

        line_cnt++;
        if(cnt==0) //loads a name of ability
        {
            if(change_format(line)=="")
           {
               std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
               throw std::runtime_error("The ability has no name.");
           }
            m_name= change_format(line);

        }
        if(cnt==1) { //loads a description of an ability
            m_description=line;
        }

        if(cnt==2) // checks whether it is active of passive ability
        {
            if(line!=PASSIVE_ABILITY && line!=ACTIVE_ABILITY)
            {
                std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
                throw std::runtime_error("The ability is not active or passive.");
            }
            if(line==ACTIVE_ABILITY) {
                m_is_active=true;
            }
        }
        if(cnt==3) //loads number that will do the effect
        {
            int stat=0;
            if(!check_int(line,stat))
            {
                std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
                throw std::runtime_error("The ability stat is not set correctly.");
            }
            if(stat<=0)
            {
                std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
                throw std::runtime_error("The ability stat is lower then 0.");
            }
            m_stat=stat;
        }
        if(cnt==4) //loads an id of an ability
        {
            int tmp = check_int_id(line_cnt,line);
            m_id=tmp;
            break;
        }
        cnt++;
    }
    if(cnt<4) //if there are less then 5 arguments (the file is shorter then it should be)
    {
        std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
        throw std::runtime_error("The ability doesn't have enough arguments");
    }
}



int CAbility::check_int_id(size_t line_cnt, std::string &line)const
{
    int tmp=0;
    if(!check_int(line,tmp))
    {
        std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
        throw std::runtime_error("The number in a dialog is not set correctly");
    }
    return tmp;
}

bool operator<(const CAbility &lhs, const CAbility &rhs)
{
    if(lhs.m_id<rhs.m_id)
        return true;
    return false;
}





