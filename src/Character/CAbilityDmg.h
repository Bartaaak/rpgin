#pragma once

#include "CAbility.h"

/**
 * This class represents an ability that deals damage to an enemy. All methods are overriding functions from CAbility.
 */
class CAbilityDmg: public CAbility
{

    void import_pass_ability([[maybe_unused]]double& hp, [[maybe_unused]]double& dmg, [[maybe_unused]] double& sh)override;
    void insert_abilities(std::vector <std::unique_ptr<CAbility>> &entities)override;
    void use_ability(CHero & player, CEnemy & enemy)override;
    void print_ability()const override ;

};


