
#include "CInventory.h"
#include "../Config/CWorldPlay.h"
#include <algorithm>


bool CInventory::add_item(const CItem &item)
{
    if(m_actuall_cap++>m_capacity)
        return false;
    auto it= (std::find_if(m_items.begin(), m_items.end(), [&](const auto& ptr) {
        return ptr->get_name() == item.get_name();}));
    if(it!=m_items.end())
        return false;

   m_items.insert(std::make_unique<CItem>(item));
    return true;
}

void CInventory::print_inventory() const
{
    for(const auto & itr: m_items )
    {
        itr->print_item();
    }
    std::cout<<std::endl;
}

