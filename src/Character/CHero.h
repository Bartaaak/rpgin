#pragma once

#include "CCharacter.h"
#include "../World/CItem.h"
#include "../utils.h"
#include <set>
#include <algorithm>
#include <sstream>
#include <set>
/**
 * This class represents a playable hero
 */
class CHero: public CCharacter
{
public:


    /**
     *  Default constructor
     */
    CHero()=default;
    /**
     *  Getter that returns name of a hero
     * @return name of a hero
     */
    const std::string get_name() const {
        return m_name;
    }
    /**
     * Getter for hp
     * @return hp
     */
    int get_hp()const{
        return m_health;
    }
    /**
     * Getter for damage
     * @return return damage
     */
    int get_dmg()const{
        return m_dmg;
    }
    /**
     * Getter for shield
     * @return shield
     */
    int get_int()const{
        return m_shield;
    }
    /**
     * Getter for abilities
     * @return abilities
     */
    const std::set<int>& get_abilities() const {
        return m_abilities;
    }
    /**
     * Operator for copying an object
     * @param[in] other to copy from
     * @return a reference to current object
     */
    CHero& operator=(const CHero& other) ;
    /**
     * Declares friend classes, so that they can see protected members
     */

    friend class CWorldPrepare;
    friend class CWorldPlay;
    friend class CCombat;
    friend class CSLGame;
    friend class CAbilityHp;
    friend class CAbilitySh;
    friend class CAbilityDmg;

    /**
     *  Creates hero based on a data from configuration file
     * @param[in] file fstream that represents a file
     * @param[in] line_cnt represents number of lines of a configuration file, so that you can see where mistake is.
     */
    void create_char(std::fstream &file, size_t &line_cnt) override;
    /**
     *  Method that read line and extracts number.
     * @param[in] line_cnt Represents number of lines in a configuration file
     * @param[in] line  string that the method reads
     * @return a number, if the input is invalid, throws and exception
     */
    int check_int_hero( size_t line_cnt,std::string & line)const;
    /**
     *  Prints out a hero on a standart output
     */
    bool read_abilities(std::string line);
    /**
     * prints out a hero
     */
    void print_char ()const override ;
protected:
    /** represents description of a hero*/
    std::string m_description;
    /** represents abilities of a hero*/
     std::set <int>m_abilities ;
     /** represents real health of a hero*/
     double m_real_health;
     /** represents real damage of a hero*/
     double m_real_dmg;
     /** represents real shield of a hero*/
     double m_real_shield;







};


