#pragma once
#include "../World/CItem.h"
#include <memory>
#include <set>
#define INVENT_CAP 20 //number of items that an be stored
/**
 * The class represents an inventory
 */
class CInventory
{
public:
    /**
     * Declares friend classes so that they can access protected members
     */
    friend class CSLGame;
    friend class CWorldPlay;

    /**
     * add item to an inventory
     * @param[in] item to be added
     * @return true if it was succesfull
     */
    bool add_item(const CItem &item);
    /**
     * getter for items
     * @return items
     */
    std::set <std::unique_ptr<CItem>>& get_items(){
        return m_items;
    }
    /**
     * prints inventory
     */
    void print_inventory()const;
protected:
    /** Capacity of inventory*/
    int m_capacity= INVENT_CAP;
    /** How many items there are*/
    int m_actuall_cap=0;
    /** Items*/
    std::set <std::unique_ptr<CItem>> m_items;

};


