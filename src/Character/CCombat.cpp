#include "../utils.h"
#include "CCombat.h"
#include <vector>
#include <algorithm>
#include <memory>
#include <unistd.h>
#define CUT "---------------------------------------------------------"


bool CCombat::fight(CWorldPlay &player, CEnemy &enemy)
{
    int tmp_real_health=player.m_player.m_real_health; //so that it can restore the health
    if(player.m_player.m_real_health<=0) //if player has died
        return false;
    if(enemy.m_health<=0) //if enemy died return true
        return true;

    std::vector<std::unique_ptr<CAbility>> tmp_abs; //abilities taht a hero can use
    while(1)
    {
        system("clear");
        print_everything(player, enemy);
        size_t choice = CWorldPlay::input(player.m_abilities.size() + 1);
        if(choice>=player.m_abilities.size()+10) // if a user wants to save a game, he cannot
        {
            std::cout<<"You cannot save now."<<std::endl;
            sleep(2);
            continue;
        }
        if (choice == player.m_abilities.size() + 1) { //if a player chose to do basic attack
            take_damage_enemy(enemy, player.m_player);
        }
        if (choice < player.m_abilities.size() + 1) { //if a player choose to use an ability
            choice--;
            auto &ab = player.m_abilities[choice];
            ab->use_ability(player.m_player,enemy);
        }
        take_damage_hero(player.m_player, enemy);
        if(player.m_player.m_real_health<=0) //if hero dies , return false
            return false;
        if(enemy.m_health<=0) {
            player.m_player.m_real_health=tmp_real_health; //if hero wins, get his hp back and return true
            return true;
        }
    }
}





void CCombat::print_friend(const CHero& hero)const
{
    std::cout<<CUT
             <<std::endl<<hero.m_name<<std::endl  << "           " << "HP: " << hero.m_real_health << "        " << "DMG: "
             << hero.m_real_dmg << "        " << "SHIELD: " << hero.m_real_shield << std::endl;

}

void CCombat::print_enemy(const CEnemy &enemy) const
{
    std::cout<<CUT
             <<std::endl<<enemy.m_name<<std::endl  << "           " << "HP: " << enemy.m_health << "        " << "DMG: "
             << enemy.m_dmg<< "        " << "SHIELD: " << enemy.m_shield << std::endl;

}



void CCombat::print_everything(const CWorldPlay & player, const CEnemy &enemy)const
{
    system("clear");
    print_friend(player.m_player);
    print_enemy(enemy);
    int cnt=1;
    for(const auto& itr1: player.m_abilities)
    {
        std::cout<<cnt<<") ";
        itr1->print_ability();
        cnt++;
    }
    std::cout<<cnt++<<") "<<"Attack"<<std::endl;

}


void CCombat::take_damage_hero(CHero &hero,CEnemy &enemy)
{
    hero.m_real_shield-=enemy.m_dmg;
    if(hero.m_real_shield<=0)
    {
        hero.m_real_health-=abs(hero.m_real_shield);
        hero.m_real_shield=0;
    }
}

void CCombat::take_damage_enemy(CEnemy &enemy,CHero &hero)
{
    enemy.m_shield-=hero.m_real_dmg;
    if(enemy.m_shield<=0)
    {
        enemy.m_health -= abs(enemy.m_shield);
        enemy.m_shield=0;
    }
}
