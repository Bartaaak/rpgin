

#include <algorithm>
#include "CAbilitySh.h"

void CAbilitySh::import_pass_ability([[maybe_unused]]double &hp, [[maybe_unused]]double &dmg, double &sh)
{
    sh*=m_stat;

}
void CAbilitySh::insert_abilities(std::vector<std::unique_ptr<CAbility>> &entities)
{
    entities.emplace_back(std::make_unique<CAbilitySh>(*this));
}

void CAbilitySh::use_ability(CHero &player, [[maybe_unused]]CEnemy &enemy)
{
    player.m_real_shield+=m_stat;

}

void CAbilitySh::print_ability() const {
    std::cout<<get_name()<<" "<<"| Shield:"<<" "<<get_stat()<<std::endl;

}
