

#include <algorithm>
#include "CAbilityDmg.h"

void CAbilityDmg::import_pass_ability([[maybe_unused]]double &hp, double &dmg, [[maybe_unused]]double &sh)
{
    dmg*=m_stat;
}

void CAbilityDmg::insert_abilities(std::vector<std::unique_ptr<CAbility>> &entities)
{
    entities.emplace_back(std::make_unique<CAbilityDmg>(*this));
}

void CAbilityDmg::use_ability([[maybe_unused]]CHero &player, CEnemy &enemy)
{
    enemy.m_shield-=m_stat;
    if(enemy.m_shield<=0)
    {
        enemy.m_health-=abs(enemy.m_shield);
        enemy.m_shield=0;
    }
}

void CAbilityDmg::print_ability() const
{
    std::cout<<get_name()<<" "<<"| Damage:"<<" "<<get_stat()<<std::endl;
}
