#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include "../Character/CHero.h"
#include "../Character/CEnemy.h"

/**
 *The class represents an ability that a hero can use.the ability can be passive or can be activated. By default the ability is passive.
 */
class CAbility {
public:
    /**
     * Getter for name of an ability.
     * @return name of an ability.
     */
    std::string get_name() const {
        return m_name;
    }
    /**
     * Getter for id of an ability.
     * @return id of an ability.
     */
    int get_id()const{
        return m_id;
    }
    /**
     * Getter for stat
     * @return number that represents a stat of ability
     */
    int get_stat()const{
        return m_stat;
    }
    /**
     * Getter for is_actove
     * @return true if an ability can be activated or false if it cannot
     */
    bool get_active()const{
        return m_is_active;
    }

    /**
     * Bool operator that can differ 2 abilities based on an id (number)
     * @param[in] lhs left ability that is to be compared
     * @param[in] rhs right ability that is to be compared
     * @return true if left ability has lower id then right ability.
     */
    friend bool operator<(const CAbility &lhs, const CAbility &rhs) ;
    /**
     *Setting friend class so that CWorldPrepare and CCombat can acces private members of CAbility
     */
    friend class CWorldPrepare;
    friend class CCombat;

    /**
     * Default,Destructor constructor of the ability.
     */
    CAbility()=default;
    /**
     * Virtual destructor
     */
    virtual ~CAbility()=default;
    /**
     * This method will create ability based on configuration file.
     * @param [in] file represents an input from configuration file, from which the ability will be configurated.
     * @param [in] line_cnt represents number of line
     */
    void create_ability(std::fstream & file,size_t & line_cnt);

    /**
     * This function will boost a hero based on his passive abilities.
     * @param hp boost health
     * @param dmg boost damage
     * @param sh boost shield
     */
    virtual void import_pass_ability([[maybe_unused]]double& hp, [[maybe_unused]]double& dmg, [[maybe_unused]] double& sh)=0;
    /**
     * Insets current ability into a given vector
     * @param entities represents vector of abilities
     */
    virtual void insert_abilities(std::vector <std::unique_ptr<CAbility>> &entities)=0;
    /**
     * When a hero uses an ability, this function is called
     * @param player a hero
     * @param enemy an ene,y
     */
    virtual void use_ability(CHero & player, CEnemy & enemy)=0;

    /**
     * Prints ability.
     */
    virtual void print_ability()const=0 ;


    /**
     * Checks if an id of an ability is correct
     * @param[in] line_cnt number of current line
     * @param[in] line string that a function reads from
     * @return if successful returns the number that is written otherwise throws an exception
     */
    int check_int_id(size_t line_cnt, std::string &line)const;
    /**
     * This function copies the contents of another CAbility object into the current object.
     * @param[in] other The CAbility object to be copied.
     * @return A reference to the modified CAbility object.
     */

protected:
    /** The name of the ability*/
    std::string m_name;
    /** The description of the ability*/
    std::string m_description;
    /** If the ability can be activated, it is true otherwise it is false*/
    bool m_is_active=false;
    /** Finally a numbers that represents influence of the ability*/
    int m_stat=1;
    /**id of ability*/
    int m_id=0;
};

