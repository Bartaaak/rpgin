#pragma once
#include "CConfig.h"
#include "CWorldPrepare.h"
#include "../Character/CInventory.h"

/**
 * The class represents a playable game
 */
class CWorldPlay:public CWorldPrepare
{
public:
    /**
     * Defines friend classes so that they can acces the protected member of this class
     */
    friend class CCombat;
    friend class CSLGame;
    /**
     * Starts a game. Everything is called from here
     * @param[in] c config object
     * @param[in] is_load if it is load_game the behaviour is different
     * @param[in] room_id room_id of a loaded game
     * @return true if it was successful
     */
    bool start_game(const CConfig & c,bool is_load,int room_id);
    /**
     * Prints stats of a hero
     */
    void print_stats();
    /**
     * Output rooms one by one
     * @param[in] itr1 Represents a room from a load file if it is a load game
     * @param[in] c config object
     * @return true if it was successful
     */
    bool output_room(const std::unique_ptr<CRoom> &itr1,const CConfig & c);
    /**
     * Output dialogs to choose from
     * @param[in] dialogs set of ids of dialogs
     * @param[in] c config object
     * @param[in] curr_room room where a player is
     * @return true if it was successful
     */
    bool output_dialogs(const std::set <int> &dialogs,const CConfig & c,const std::unique_ptr<CRoom> &curr_room);
    /**
     * Loads data from CWorldCheck
     * @param[in] c config object
     * @param[in] player hero of a player
     * @param[in] abilities hero's abilities
     * @param[in] pq heroes quotients
     */
    void translate(const CConfig & c,CHero  & player,const std::vector <std::unique_ptr<CAbility>> &abilities,const player_quotient & pq);
    /**
     * Changes stats based on an item
     * @param[in] item the item to change stats from
     */
    void change_stats(const CItem & item);
    /**
     * update stats based on quotients
     */
    void update_real_stat();
    /**
     * loads data if it is a loaded game
     * @param[in] c config object
     * @param[in] player hero of a user
     * @param[in] abilities hero's abilities
     * @param[in] pq hero's quotients
     * @param[in] room_id the room_id where he is
     * @param[in] inventory his inventory
     */
    void translate_load(const CConfig &c, CHero &player, const std::vector <std::unique_ptr<CAbility>> &abilities, const player_quotient &pq,
                        int room_id, CInventory& inventory);

protected:
    /**Inventory of a hero*/
    CInventory m_inventory;
    /**Used if it is a load_file*/
    bool is_ready=false;


};


