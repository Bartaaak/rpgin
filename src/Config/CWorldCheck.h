#pragma once
#include <string>
#include "CConfig.h"
/**
 * Checks whether the object from config file can be created
 */
class CWorldCheck: public CConfig
{
public:
    /**
     * Default constructor
     */
    CWorldCheck()=default;
    /**
     * Checks whole instance if it can be created
     * @param[in] c config object
     * @return true if it is successful
     */
    bool check_world( const CConfig & c)const;
    /**
     * Check if the abilities are compatible with heroes
     * @param[in] c config object
     * @return true if it is successful
     */
    bool check_hero_abilities(const CConfig &c)const;
    /**
     * Check whether dialogs can be created
     * @param[in] c config object
     * @return true if it is successful
     */
    bool check_dialogs(const CConfig &c)const;
    /**
     * Check whether rooms can be created
     * @param[in] c config object
     * @return
     */
    bool check_rooms(const CConfig &c)const;
};


