#pragma once
#include "../utils.h"
#include "../Character/CAbility.h"
#include "../Character/CHero.h"
#include "../Character/CEnemy.h"
#include "../World/CItem.h"
#include "../World/CDialog.h"
#include "../World/CRoom.h"
#include <memory>
#include <vector>
    /**
     *  The class represents the process of reading the config file.
     */
class CConfig {
public:
    /**
     * Getter for vector of heroes
     * @return vector of heroes
     */
    const std::vector<std::unique_ptr<CHero>>& get_heroes() const {
        return v_heroes;
    }
    /**
     * Getter for vector of enemies
     * @return vector of enemies
     */
    const std::vector<std::unique_ptr<CEnemy>>& get_enemies() const {
        return v_enemies;
    }
    /**
     * Getter for vector of items.
     * @return vector of items.
     */
    const std::vector<std::unique_ptr<CItem>>& get_items() const {
        return v_items;
    }
    /**
     * Getter for vector of dialogs
     * @return vector of dialogs
     */
    const std::vector<std::unique_ptr<CDialog>>& get_dialogs() const {
        return v_dialogs;
    }
    /**
     * Getter for vector of rooms
     * @return vector of rooms
     */
    const std::vector<std::unique_ptr<CRoom>>& get_rooms() const {
        return v_rooms;
    }
    /**
     * Getter for vector of abilities
     * @return vector of abilities
     */
    const std::vector<std::unique_ptr<CAbility>>& get_abilities() const {
        return v_abilities;
    }
    /**
     * Int that represent number of current line
     * @return number of a line
     */
    int get_line_cnt () const{
        return line_cnt;
    }
    /**
     * Creates an ability based on a type
     * @param file config
     * @param line_cnt number of line
     * @param read_me ability from config
     * @return
     */
    bool decide_ability(std::fstream & file,size_t & line_cnt,std::string & read_me);
    /**
     * Function that will read from config and load the entities
     */
    void read_config();
    /**
     * prints out all the entities
     */
    void print_entities()const;

    /**
     * Insert entity no a vector of entities, it doesnt care abou a type
     * @param[in] entities vector of entities
     * @param[in] entity to be insert
     * @return true if it was successful
     */
    template<typename VectorT, typename EntityT>
    bool insert_entity(VectorT& entities, const EntityT& entity);

    /**
     * Insert body (dialog or room)
     * @param[in] entities vector of rooms or dialogs
     * @param[in] entity room or dialog to be inserted
     * @return true if it was successful s
     */
    template<typename T>
    static bool insert_body(std::vector<std::unique_ptr<T>>& entities, const T& entity);



protected:
    /**Vector of CBody (CRoom or CDialog) that are in one vector*/
    std::vector <std::unique_ptr<CBody>> v_bodies;
    /** Vector of v_chars (CHero or CEnemy) that are in one vector*/
    std::vector <std::unique_ptr<CCharacter>> v_chars;
    /** Storage for heroes*/
    std::vector <std::unique_ptr<CHero>> v_heroes;
    /**Storage for enemies*/
    std::vector <std::unique_ptr<CEnemy>> v_enemies;
    /**Storage for items*/
    std::vector <std::unique_ptr<CItem>> v_items;
    /**Storage for dialogs*/
    std::vector <std::unique_ptr<CDialog>> v_dialogs;
    /**Storage for rooms*/
    std::vector <std::unique_ptr<CRoom>> v_rooms;
    /**Represents number of lines*/
    size_t line_cnt=0 ;//represents number of lines in a configuration file;
    /**Storage for abilities*/
    std::vector <std::unique_ptr<CAbility>> v_abilities;



};


