#include "CSLGame.h"
#include <fstream>
#include <memory>
#define PATH_TO_SAVE "../bartami8/src/config_files/save.txt"
#include "CConfig.h"

#include "../utils.h"
bool CSLGame::save_game(const std::unique_ptr<CRoom> &curr_room, CWorldPlay & game)
{
    std::ofstream file(PATH_TO_SAVE);
    if (file.fail())
    {
        return false;
    }
        if (file.is_open()) //opens a file and write data about the instance
    {
        file << curr_room->get_id()<<std::endl;
        file << game.m_player.get_name()<<std::endl;;
        file << game.m_player.m_real_health<<std::endl;
        file << game.m_player.m_real_dmg<<std::endl;
        file << game.m_player.m_real_shield<<std::endl;
        file << game.m_player.m_health<<std::endl;
        file << game.m_player.m_dmg<<std::endl;
        file << game.m_player.m_shield<<std::endl;
        file << game.m_quot.m_hp_quoc<<std::endl;
        file << game.m_quot.m_sh_quoc<<std::endl;
        file << game.m_quot.m_dmg_quoc<<std::endl;
        for(const auto & itr1: game.m_abilities)
        {
            if(itr1->get_active())
                file << itr1->get_name()<<std::endl;
        }
        for(const auto & itr1: game.m_inventory.get_items())
        {
            file << itr1->get_name()<<std::endl;
        }
        file.close(); //clost the file and return true if it was sucesfull
        return true;
    }
    else
    {
        return false;
    }
}

void CSLGame::load_game(CConfig &config) //please dont kill me
{

    std::fstream file(PATH_TO_SAVE); //Opens a save file
    if (file.fail())
        throw std::runtime_error("Save:Cannot open a file.");
    std::string read_me;
    int cnt = 0;
    CHero player;
    const std::vector <std::unique_ptr<CAbility>> abilities;
    CWorldPlay::player_quotient pq;
    int id_room = 0;
    int tmp = 0;
    CInventory player_inv;
    std::vector <std::unique_ptr<CAbility>> m_abilities;
    while (std::getline(file, read_me)) { //reads from a save file
        if(change_format(read_me)=="")
            continue;
        if (cnt == 0) {
            check_int(read_me, id_room);
            auto it = std::find_if(config.get_rooms().begin(), config.get_rooms().end(),
                                   [&id_room](const std::unique_ptr<CRoom> &d) {
                                       return d->get_id() == id_room;
                                   });
            if (!check_int(read_me, id_room) || it == config.get_rooms().end())
                throw std::runtime_error("Save:The id of a room is wrong");
        }
        if (cnt == 1) {
            std::string find = change_format(read_me);
            auto it = std::find_if(config.get_heroes().begin(), config.get_heroes().end(),
                                   [&find](const std::unique_ptr<CHero> &d) {
                                       return d->get_name() == find;
                                   });
            if (it == config.get_heroes().end())
                throw std::runtime_error("Save:The hero does not exist");
            player.m_name = find;

        }
        if (cnt == 2) {
            if (!check_int(read_me, tmp))
                throw std::runtime_error("Save:The real_health is not set correctly");
            player.m_real_health = tmp;

        }
        if (cnt == 3) {
            if (!check_int(read_me, tmp))
                throw std::runtime_error("Save:The real_dmg is not set correctly");
            player.m_real_dmg = tmp;
        }
        if (cnt == 4) {
            if (!check_int(read_me, tmp))
                throw std::runtime_error("Save:The real_shield is not set correctly");
            player.m_real_shield = tmp;
        }
        if (cnt == 5) {
            if (!check_int(read_me, tmp))
                throw std::runtime_error("Save:The health is not set correctly");
            player.m_health = tmp;
        }
        if (cnt == 6) {
            if (!check_int(read_me, tmp))
                throw std::runtime_error("Save:The damage is not set correctly");
            player.m_dmg = tmp;
        }
        if (cnt == 7) {
            if (!check_int(read_me, tmp))
                throw std::runtime_error("Save:The shield is not set correctly");
            player.m_shield = tmp;
        }
        if (cnt == 8) {
            if (!check_int(read_me, tmp))
                throw std::runtime_error("Save:The hp quotient is not set correctly");
            pq.m_hp_quoc = tmp;
        }
        if (cnt == 9) {
            if (!check_int(read_me, tmp))
                throw std::runtime_error("Save:The int quot is not set correctly");
            pq.m_sh_quoc = tmp;

        }
        if (cnt == 10) {
            if (!check_int(read_me, tmp))
                throw std::runtime_error("Save:The dmg quot is not set correctly");
            pq.m_dmg_quoc = tmp;
            break;
        }
        cnt++;
    }
    if (cnt < 10)
        throw std::runtime_error("Save:Not enough arguments");
    while (std::getline(file, read_me)) //reads items and abilities
    {
        change_format(read_me);
        auto it1=std::find_if(config.get_items().begin(),config.get_items().end(),[&read_me](const std::unique_ptr<CItem> &d){
           return d->get_name()==read_me;
        });
        auto it2=std::find_if(config.get_abilities().begin(),config.get_abilities().end(),[&read_me](const std::unique_ptr<CAbility> &d){
            return d->get_name()==read_me;
        });
        if(it1==config.get_items().end() && it2==config.get_abilities().end())
            throw std::runtime_error("Save:Invalid ability or item");
        if(it1!=config.get_items().end() && it2!=config.get_abilities().end())
            throw std::runtime_error("Save:An item and an ability has same name");
        if(it1!=config.get_items().end())
            player_inv.add_item(*(*it1));
        if(it2!=config.get_abilities().end())
        {
            (*it2)->insert_abilities(m_abilities);
        }
    }
    CWorldPlay game_on;
    game_on.translate_load(config, player, m_abilities, pq, id_room, player_inv); //loads a game
}

