
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <memory>
#include "CConfig.h"
#include "../Character/CAbilityHp.h"
#include "../Character/CAbilityDmg.h"
#include "../Character/CAbilitySh.h"

#define CUT "---------------------------------------------------------"
#define PATH_TO_CONFIG "../bartami8/src/config_files/configure_me.txt" //declares a path to a config file
#define CONFIG_KEYWORD "#!/start/config" //This keyword has to be the first word in a configuration file
#define ABILITY_HP "ABH" // Keyword used for health ability
#define ABILITY_DMG "ABD" // Keyword used for damage ability
#define ABILITY_SH "ABSH" // Keyword used for shield ability
#define HERO "HE" // Keyword used for hero
#define ENEMY "EN"  // Keyword used for enemy
#define ITEM "I" //Keyword used for item
#define ROOM "R"  //Keyword used for a room
#define DIALOG "D" //Keyword used for a dialog

void CConfig::read_config() { //reads from config
    std::fstream file(PATH_TO_CONFIG); //Opens a config file
    if (file.fail())
        throw std::runtime_error("Config:Cannot open a file.");
    std::string read_me;
    file >> read_me;
    if (read_me != CONFIG_KEYWORD) // if there is not a keyword in the first line of a file. Throw an exception
        throw std::runtime_error("Config:Missing keyword at the beginning.");
    line_cnt++;
    while (std::getline(file, read_me)) //reads from a config file
    {
        read_me= change_format(read_me);
        line_cnt++;
        if (read_me == ABILITY_DMG || read_me==ABILITY_HP || read_me==ABILITY_SH) //if the ability keyword is detected, create ability
        {
            decide_ability(file, line_cnt,read_me);
        }
        if (read_me == HERO) //if it is a hero, create hero and insert
        {
            CHero hero;
            hero.create_char(file,line_cnt);
            if(!insert_entity(v_heroes,hero))
            {
                std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
                throw std::runtime_error("At least 2 heroes have same name.");
            }
            v_chars.emplace_back(std::make_unique<CHero>(hero));
        }
        if(read_me==ENEMY) //if it is an enemy, create enemy and insert it
        {
            CEnemy enemy;
            enemy.create_char(file,line_cnt);
            if(!insert_entity(v_enemies,enemy))
            {
                std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
                throw std::runtime_error("At least 2 enemies have same name.");
            }
            v_chars.emplace_back(std::make_unique<CEnemy>(enemy));


        }
        if(read_me==ITEM) // if it is an item, create item and insert it
        {
            CItem item;
            item.create_item(file,line_cnt);
            if(!insert_entity(v_items,item))
            {
                std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
                throw std::runtime_error("At least 2 items have same name.");
            }
        }
        if(read_me==ROOM) //if it is a room, create room and insert
        {
            CRoom room;
            room.create_body(file,line_cnt);
            if(!insert_body(v_rooms,room))
            {
                std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
                throw std::runtime_error("At least 2 rooms have same id.");
            }
            v_bodies.emplace_back(std::make_unique<CRoom>(room));

        }
        if(read_me==DIALOG) //if it is a dialog, create dialog and insert it
        {
            CDialog dialog;
            dialog.create_body(file,line_cnt);
            if(!insert_body(v_dialogs,dialog))
            {
                std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
                throw std::runtime_error("At least 2 dialogs have same id.");
            }
            v_bodies.emplace_back(std::make_unique<CDialog>(dialog));

        }
        if(read_me!=ABILITY_SH && read_me!=ABILITY_DMG && read_me!=ABILITY_HP && read_me != HERO && read_me!= ENEMY && read_me!= ITEM && read_me!=ROOM && read_me!=DIALOG&& !read_me.empty())
            //if it is not a keyword and it is not empty, its invalid keyword, throw
        {
            std::cout<<std::endl<<"Config:A problem near line "<< line_cnt<<std::endl;
            throw std::runtime_error("Invalid keyword.");
        }
    }


}



void CConfig::print_entities()const //prints all entites in configuration file
{
    for(const auto & itr1: v_chars) //polymorphism
    {
        itr1->print_char();
    }
    std::cout<<CUT<<std::endl;
    for(const auto & itr1: v_bodies) //polymorphism
    {
        itr1->print_body();
    }
    std::cout<<CUT<<std::endl;

    for(const auto & itr1: v_abilities)
    {
        itr1->print_ability();
    }
    std::cout<<CUT<<std::endl;

    for(const auto & itr1: v_items)
    {
        itr1->print_item();
    }
    std::cout<<CUT<<std::endl;

}

bool CConfig::decide_ability(std::fstream & file,size_t & line_cnt,std::string & read_me)
{
    if(read_me==ABILITY_HP)
    {
        CAbilityHp abilityHp;
        abilityHp.create_ability(file, line_cnt);
        insert_entity(v_abilities, abilityHp);
    }
    if(read_me==ABILITY_DMG)
    {
        CAbilityDmg abilityDmg;
        abilityDmg.create_ability(file, line_cnt);
        insert_entity(v_abilities, abilityDmg);
    }
    if(read_me==ABILITY_SH)
    {
        CAbilitySh abilitySh;
        abilitySh.create_ability(file, line_cnt);
        insert_entity(v_abilities, abilitySh);
    }

    return false;
}



template<typename VectorT, typename EntityT>
bool CConfig::insert_entity(VectorT& entities, const EntityT& entity) {
    auto it = std::find_if(entities.begin(), entities.end(), [&entity](const auto& existingEntity) {
        return existingEntity->get_name() == entity.get_name();
    });

    if (it != entities.end()) {
        return false;
    }

    entities.insert(std::lower_bound(entities.begin(), entities.end(), entity,
                                     [](const auto& a, const auto& b) {
                                         return a->get_name() < b.get_name();
                                     }), std::make_unique<EntityT>(entity));
    return true;
}
template<typename T> //insert body based on a type
bool CConfig::insert_body(std::vector<std::unique_ptr<T>>& entities, const T& entity)
{
    auto it = std::find_if(entities.begin(), entities.end(), [&entity](const auto& existingEntity) {
        return existingEntity->get_id() == entity.get_id();
    });
    if (it != entities.end())
        return false;
    entities.insert(std::lower_bound(entities.begin(), entities.end(), entity,
                                     [](const std::unique_ptr<T>& a, const T& b) {
                                         return a->get_id() < b.get_id();
                                     }), std::make_unique<T>(entity));
    return true;
}


