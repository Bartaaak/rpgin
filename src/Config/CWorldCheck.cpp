
#include "CWorldCheck.h"
#include <algorithm>
#include "CWorldPrepare.h"



bool CWorldCheck::check_world(const CConfig &c)const
{
    if(c.get_heroes().size()<=0 || c.get_dialogs().size()<=0 || c.get_rooms().size()<=0) //if there are no heroes or dialogs or rooms throw an exception
    {
        throw std::runtime_error("Config:Some entities are missing");
    }
    if(!(check_hero_abilities(c))) //if the abilities are not compatible with heroes throw an exception
    {
        throw std::runtime_error("Config:The abilities of heroes are not set correctly");
    }
    if(!(check_dialogs(c))) //if the dialogs cannot be created, throw an exception
    {
        throw std::runtime_error("Config:The dialogs are not set correctly");
    }
    if(!(check_rooms(c))) //if the rooms cannot be created, throw an exception
    {
        throw std::runtime_error("Config:The dialogs are not set correctly");
    }
    return true;
}

bool CWorldCheck::check_hero_abilities(const CConfig &c) const // checks whether heroes and abilities are compatible
{
    for (const auto &itr1 : c.get_heroes())
    {
        for (const auto &itr2 : itr1->get_abilities())
        {
            auto it = std::find_if(c.get_abilities().begin(), c.get_abilities().end(), [&itr2](const auto& ability) {
                return ability->get_id() == itr2;
            });

            if (it == c.get_abilities().end())
                return false;

        }
    }
    return true; // All abilities do exist
}

bool CWorldCheck::check_dialogs(const CConfig & c) const // checks whether dialogs can be created.
{
    for(const auto &itr1:c.get_dialogs())
    {
        if(itr1->is_combat()) //if it is combat, checks whether there is an enemy to defeat
        {
            auto it=std::find_if(c.get_enemies().begin(),c.get_enemies().end(),[&itr1](const auto& enemy){
                return enemy->get_name()==itr1->get_action();
            });
            if (it == c.get_enemies().end())
                return false;

        }
        if(itr1->is_pick_up()) //if it is pick up, checks whether there is an item to pick up
        {
            auto it=std::find_if(c.get_items().begin(),c.get_items().end(),[&itr1](const auto& item){
                return item->get_name()==itr1->get_action();
            });
            if (it == c.get_items().end())
                return false;
        }
    }

    return true;
}

bool CWorldCheck::check_rooms(const CConfig &c) const //checks room and whether the ddialog inside of it exists
{
    for(const auto & itr1: c.get_rooms())
    {
        if(itr1->get_dialogs().size() ==0)
            throw std::runtime_error("The room has no dialogs");

        for(const auto & itr2: itr1->get_dialogs())
        {
            auto it = std::find_if(c.get_dialogs().begin(), c.get_dialogs().end(), [&itr2](const auto& dialog) {
                return dialog->get_id() == itr2;
            });

            if (it == c.get_dialogs().end())
                return false;
        }
    }
    return true;
}

