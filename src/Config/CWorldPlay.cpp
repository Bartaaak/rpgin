#include "CWorldPlay.h"
#include "../Character/CCombat.h"
#include "CConfig.h"
#include "CSLGame.h"
#include <cstdlib>
#include <memory>
#define MIN_C 1
#define PASSIVE_ABILITY 0 // Sets ability to be passive
#define ACTIVE_ABILITY 1 // Sets ability to be active
#define CUT "---------------------------------------------------------"
bool CWorldPlay::start_game(const CConfig & c,bool is_load,int room_id) //starts a game
{
    if(!is_load) // if you would keel loading the game you would have infinite stat glitch
        update_real_stat();
    for(const auto &itr1:c.get_rooms())
    {
        if(is_load && itr1->get_id()==room_id && !is_ready)  //if it is a load game, keep going and find the room where the hero was
        {
            is_ready= true;
        }
        if(is_ready || !is_load)
        {
            while (true) //normal game
            {
                if (output_room(itr1, c)) //if it was last room, end the game
                    break;
            }
        }
        system("clear");
    }
    std::cout<<"Vae victis"<<std::endl;

    return true;
}
void CWorldPlay::print_stats()//prints stats of a hero
{

    std::cout<<CUT
             <<std::endl<<m_player.m_name<<std::endl  << "           " << "HP: " << m_player.m_real_health << "        " << "DMG: "
             << m_player.m_real_dmg << "        " << "SHIELD: " << m_player.m_real_shield << std::endl;
    m_inventory.print_inventory();
}
bool CWorldPlay::output_room(const std::unique_ptr<CRoom> &itr1, const CConfig & c)
{
    update_real_stat();
    std::cout<<CUT<<
             std::endl<<itr1->get_show()<<std::endl<<"-"<<itr1->get_text()<<"-"<<std::endl
             <<CUT <<std::endl;
    print_stats();
    if(output_dialogs(itr1->get_dialogs(),c,itr1))
        return true;

    return false;
}
bool CWorldPlay::output_dialogs(const std::set<int> &dialogs, const CConfig & c,const std::unique_ptr<CRoom> &curr_room)
{
    int cnt=0;
    std::vector <std::unique_ptr<CDialog>> tmp1;
    CDialog tmp2;
    for (const auto &itr2 : dialogs) //prints out dialogs to choose from
    {

        auto it = std::find_if(c.get_dialogs().begin(), c.get_dialogs().end(), [&itr2](const std::unique_ptr<CDialog>& d) {
            return d->get_id() == itr2;
        });
        tmp2.m_show=(*it)->m_show;tmp2.m_type=(*it)->m_type;tmp2.m_action=(*it)->m_action;tmp2.m_text=(*it)->m_text;
        tmp1.emplace_back(std::make_unique<CDialog>(tmp2));
        std::cout<<cnt+1<<") "<<(*it)->get_show()<<std::endl;
        cnt++;
    }
    int choice=0;
    while (true) //user chooses a dialog
    {
        choice = input(cnt);
        if (choice >= (cnt += 10)) {
            CSLGame save;
            if (!save.save_game(curr_room, (*this)))
                std::cout << "Cannot save the game" << std::endl;
            continue;
        }
        break;
    }
    std::cout<<choice<<std::endl;
    choice-=MIN_C;
    auto & c_dialog=tmp1[choice];
    const std::string & find=c_dialog->get_action();
    if(c_dialog->is_pick_up()) //if it is a pick up dialog
    {
         auto it = std::find_if(c.get_items().begin(), c.get_items().end(), [&find](const std::unique_ptr<CItem>& d) {
            return d->get_name() == find;});
       if( !(m_inventory.add_item((*(*it)))))
       {
           system("clear"); //clears out the terminal
           std::cout<<"You cannot take this item"<<std::endl;
           return false;
       }
       else
        {
            system("clear"); //clears out the terminal
            change_stats(*(*it));
            std::cout<<c_dialog->m_text<<std::endl;
            return false;
        }
    }

    if(c_dialog->is_combat()) //if it is a combat dialog, start combat
    {
        auto it = std::find_if(c.get_enemies().begin(), c.get_enemies().end(), [&find](const std::unique_ptr<CEnemy>& d) {
            return d->get_name() == find;});
        if((*(*it)).get_health()<=0) //if an enemy is already dead
        {
            system("clear");
            std::cout<<"The enemy is dead"<<std::endl;
            return false;
        }
        CCombat combat;
        if(!(combat.fight(*this,*(*it))))
        {
            system("clear"); //clears out the terminal
            throw std::runtime_error("You have died");
        }
        system("clear");
        return false;
    }
    if(c_dialog->is_next_room())
        return true;
    if(c_dialog->is_plain())
    {
        system("clear");
        std::cout<<c_dialog->m_text<<std::endl;
        return false;
    }
    return false;
}

void CWorldPlay::translate(const CConfig & c,CHero &player, const std::vector <std::unique_ptr<CAbility>> &abilities, const player_quotient &pq)
{
    m_quot.m_dmg_quoc=pq.m_dmg_quoc; m_quot.m_sh_quoc=pq.m_sh_quoc;m_quot.m_hp_quoc=pq.m_hp_quoc; //reads the date
    m_player=player;
    for(const auto &itr1:abilities) {

        (*itr1).insert_abilities(m_abilities);
    }
    start_game(c,false,0);
}

void CWorldPlay::change_stats(const CItem & item)
{

        m_player.m_real_health+=item.m_health;
        m_player.m_real_dmg+=item.m_dmg;
        m_player.m_real_shield+=item.m_shield;

}

void CWorldPlay::update_real_stat()
{
    m_player.m_real_health+=m_player.m_health*m_quot.m_hp_quoc;
    m_player.m_real_dmg+=m_player.m_dmg*m_quot.m_dmg_quoc;
    m_player.m_real_shield+=m_player.m_shield*m_quot.m_sh_quoc;
}

void CWorldPlay::translate_load(const CConfig &c, CHero &player,const std::vector <std::unique_ptr<CAbility>> &abilities,
                                const player_quotient &pq,
                                int room_id, CInventory& inventory)
{
    {
        m_quot.m_dmg_quoc=pq.m_dmg_quoc; m_quot.m_sh_quoc=pq.m_sh_quoc;m_quot.m_hp_quoc=pq.m_hp_quoc;
        m_player=player;
        for(const auto &itr1:abilities) {
            (*itr1).insert_abilities(m_abilities);
        }

        for(const auto & itr1: inventory.get_items())
        {
            m_inventory.add_item(*itr1);
            m_inventory.m_actuall_cap++;
        }
        system("clear");
        start_game(c,true,room_id);
    }

}

