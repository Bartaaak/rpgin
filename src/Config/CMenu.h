#pragma once
#include <string>
#include "CWorldCheck.h"

/**
 * This class represents a game menu. It will be able to print out the possibilities and also accept an input from a user. It will do an action based on the
 * choice  a user makes.
 */
class CMenu {
public:
   /**
    * // The method will be used everytime the engine starts. It will print a menu and it will wait for a user to choose next action
    */
    void start_eng()const;

};


