
#include "CWorldPrepare.h"
#include "CWorldPlay.h"
#include <cstdlib>
#include <memory>
#define MIN_C 1
#define PASSIVE_ABILITY 0 // Sets ability to be passive
#define ACTIVE_ABILITY 1 // Sets ability to be active
#define CUT "---------------------------------------------------------"
void CWorldPrepare::play_world(const CConfig &c)
{

    system("clear"); //clears out the terminal
    show_heroes(c); //show playable characters
    system("clear"); //clears out the terminal
    CWorldPlay game;
    game.translate(c,m_player,m_abilities,m_quot); //starts a game

}

int CWorldPrepare::input(int cnt_heroes) {
    std::string line;
    int input=0;

    while (getline(std::cin,line)) {


        if(change_format(line)=="save") //you cannot save a game in world prepare
            return (cnt_heroes+=30);

        if(valid_input(line)) //reads input from a user
        { //checks if the input is valid
            std::istringstream iss(line);
            iss>>input;
            if(input>=MIN_C && input<=cnt_heroes) {
                return input;
            }
        }
        std::cout << "Type only numbers "<<MIN_C<<") to " <<cnt_heroes<<"), PLEASE. For example: "<<MIN_C<<") (you must use a close bracket after a number)"
        <<std::endl;
    }
    throw std::runtime_error("Wrong input by user.");
}



void CWorldPrepare::show_heroes(const CConfig & c) //show all heroes from config file
{
    int i=0;
    std::cout<<CUT<<std::endl;
    for(const auto &itr1: c.get_heroes())
    {
        i++;
        std::cout<<std::endl<<i<<") ";
        itr1->print_char();
        std::cout<<"   Abilities:"<<std::endl;
        for(const auto &itr2: itr1->get_abilities())
        {
            auto it = std::find_if(c.get_abilities().begin(), c.get_abilities().end(), [&itr2](const auto& ability) {
                return ability->get_id() == itr2;
            });
            (*it)->print_ability();

        }

        std::cout<<CUT<<std::endl;
    }
    std::cout<<"Choose your hero:"<<std::endl;
    int pos_hero=0;
    while(1)
    {
        pos_hero=input(i);
        if(pos_hero<=(i++))
            break;
        std::cout<<"You cannot save game now."<<std::endl;
    }

    if( pos_hero-MIN_C>(int)c.get_heroes().size()-1) //if it was somehow bad, throw an exception
        throw std::runtime_error("Unexpected. TERMINATING........");
    pos_hero-=MIN_C;
    m_player.m_name = c.get_heroes()[pos_hero]->get_name(); //sets a hero
    m_player.m_health = c.get_heroes()[pos_hero]->get_hp();
    m_player.m_dmg= c.get_heroes()[pos_hero]->get_dmg();
    m_player.m_shield= c.get_heroes()[pos_hero]->get_int();

    import_abilities(c, pos_hero); //imports abilities of a hero
}

void CWorldPrepare::import_abilities(const CConfig &c, const size_t pos_hero)
{
    m_quot.m_hp_quoc=1;m_quot.m_sh_quoc=1;m_quot.m_dmg_quoc=1; //Initializing quotient

    for(const auto & itr1: c.get_heroes()[pos_hero]->get_abilities() ) //reads passive and active abilities
    {
        auto it = std::find_if(c.get_abilities().begin(), c.get_abilities().end(), [&itr1](const auto& ability) {
            return ability->get_id() == itr1;
        });
        if((*it)->get_active()==PASSIVE_ABILITY)
        {
            (*it)->import_pass_ability(m_quot.m_hp_quoc, m_quot.m_dmg_quoc, m_quot.m_sh_quoc);

        }
        if((*it)->get_active()==ACTIVE_ABILITY)
        {
            (**it).insert_abilities(m_abilities);

        }

    }

}




