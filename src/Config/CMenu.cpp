
#include "CMenu.h"
#include "iostream"
#include "CConfig.h"
#include "CWorldPrepare.h"
#include "CSLGame.h"
#include <string>
#include <sstream>
#include <stdexcept>
#define PATH_TO_HELP "../bartami8/src/config_files/help_config.txt"
#define MIN_C 1 // Symbolic constants that is used for range of choices in a menu. This is the first one
#define MAX_C 3 // this is the last one



void CMenu::start_eng() const{

    std::cout << std::endl<<"The RPGin game engine greets you." << std::endl << "Choose next action by typing numbers "<<MIN_C<<") to "<<MAX_C<<") "
              << std::endl;
    std::cout << std::endl << "1) New game" << std::endl << "2) Load game" << std::endl << "3) Help with config"
              << std::endl << "Type in:"<<std::endl;
    std::string line;
    int input=0;
    while (getline(std::cin,line)) { //cycle that is reading an input from user

        if("print"== change_format(line)) //prints out all entities from config and exits the game
        {
            CConfig print_config;
            print_config.read_config();
            print_config.print_entities();

        }
        if (valid_input(line)) { //checks if the input is valid
            std::istringstream iss(line);
            iss>>input;
            if(input>=MIN_C && input<=MAX_C)
                break;
        }
        std::cout << "Type only numbers "<<MIN_C<<") to "<<MAX_C<<") , PLEASE. For example: "<<MIN_C<<") (you must use a close bracket after a number)"<<std::endl;
    }

    if(input<MIN_C || input>MAX_C) // just to be sure, that it is truly valid.
        throw std::runtime_error("Wrong input by user.");

    CConfig configure; // If the input from the user is valid. The configure class will handle the creation of the world from a config file

    if(input==1) // A user chose new world.
    {
        configure.read_config();
        CWorldCheck world;
        if(!(world.check_world( configure)))
        {
            throw std::runtime_error("The creation of a world was not successful.");
        }
        CWorldPrepare prep_world;
        try
        {
            prep_world.play_world((configure));
        } catch (const std::runtime_error& er)
        {
            std::cout<<er.what()<<std::endl;
        }


    }
    if(input==2) //user chose to load a game
    {
        configure.read_config();
        CWorldCheck world;
        if(!(world.check_world( configure)))
        {
            throw std::runtime_error("The creation of a world was not successful.");
        }
        CSLGame load;
        try
        {
            load.load_game(configure);
        } catch (const std::runtime_error& er)
        {
            std::cout<<er.what()<<std::endl;
        }
    }
    if(input==3) //user chose to print out the config
    {
        std::fstream file(PATH_TO_HELP); //Opens a config file
        if (file.fail())
            throw std::runtime_error("Config:Cannot open a file.");
        while (std::getline(file, line)) {
            std::cout << line << std::endl;
        }

        file.close();
    }
}

