#pragma once
#include "CConfig.h"
#include "../Character/CAbility.h"
#include "../World/CItem.h"
#include "../Character/CCharacter.h"
#include "../utils.h"
/**
 * The class prepared world to be created
 */
class CWorldPrepare
{
public:
    /**
     * Simple method that manages whole procces and after preperation, call a method in another class to create a game
     * @param[in] c config object
     * @return true if it was successful
     */
    void play_world(const CConfig & c);
    /**
     * Reads input from a user and write an error message if it false
     * @param[in] cnt_heroes counter of heroes
     */
    static int input(int cnt_heroes);
    /**
     * Prints out possible heroes to play
     * @param[in] c config object
     */
    void show_heroes(const CConfig & c);
    /**
     * Imports abilities of a hero
     * @param[in] c config object
     * @param[in] pos_hero position of a hero inside a vector of heroes
     */
    void import_abilities(const CConfig & c, const size_t pos_hero);


protected:
    /**
     * Struct for passive abilities (the passive abilities changes quotient)
     */
    typedef struct {
        /**hp passive ability*/
        double m_hp_quoc;
        /**dmg passive ability*/
        double m_dmg_quoc;
        /**shield passive ability*/
        double m_sh_quoc;
    } player_quotient;
    /**
     * represents passive ability of hero
     */
    player_quotient m_quot;

    /**
     * Set of abilities of a player
     */
    std::vector <std::unique_ptr<CAbility>> m_abilities;
    /**
     * the player itself
     */
    CHero m_player;
};


