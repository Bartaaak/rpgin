#pragma once
#include <memory>
#include "CWorldPlay.h"
/**
 * This class will deal with saving and loading a game
 */
class CSLGame
{

public:
    /**
     * This method saves a game
     * @param[in] curr_room  where an user is
     * @param[in] game CWorldPlay object with all the info that we need
     * @return true if it was successful
     */
    bool save_game(const std::unique_ptr<CRoom> &curr_room, CWorldPlay & game);
    /**
     * This method loads a game
     * @param[in] config file
    */
    void load_game(CConfig & config);


};
