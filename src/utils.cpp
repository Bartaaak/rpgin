#include "utils.h"

bool check_int(std::string & line,int & change_me)
{
    std::stringstream check (line);
    int final=0;
    if(!(check>>final))
        return false;
    change_me=final;
    return true;
}

std::string change_format(std::string & name)
{

    auto new_end = std::unique(name.begin(), name.end(),            //it sends whitespaces to the back
                               [](char c1, char c2) { return c1 == ' ' && c2 == ' '; });
    name.erase(new_end, name.end()); //deletes the whitespaces in the back
    if (name.front() == ' ')
        name.erase(name.begin());
    if (name.back() == ' ')
        name.pop_back();
    return name;
}
bool valid_input(const std::string &check_me)  {
    std::stringstream check(check_me);
    int number;
    char bracket;
    if (!(check >> number))  //If the first char is not a number return false
        return false;

    if (!(check >> bracket) || bracket != ')') //if the second char is not a bracket return false
        return false;

    char c;
    if (check >> c)
        return false;
    return true;
}

