#pragma once
#include <string>
#include <sstream>
#include <algorithm>
#include <memory>

/**
 * @brief The function just checks a string whether there is actually an integer and if yes it returns true and sets change_me to a number in a line
 * @param[in] line String that is to be checked
 * @param[in] change_me An int that is to be changed to a number in line
 * @return If it is succesfull returns true, otherwise false
 */
bool check_int(std::string & line,int & change_me);


/**
 * @brief The function changes a string and deletes the spaces that are not necessary
 * @param[in] name The spaces will be removed from this string
 * @return the modified string
 */
std::string change_format(std::string & name);
/**
 * Valids input and if it valid return true
 * @param check_me from a string
 * @return true if it was successful
 */
bool valid_input(const std::string &check_me);

/**
 * checks whether a string is full of whitespaces?
 * @param s string
 * @return true if yes
 */
bool is_whitespace(const std::string& s) ;


template<typename VectorT, typename AbilityT>
bool insert_Ability(VectorT& abilities, const AbilityT& ability);