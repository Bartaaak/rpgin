
#include <iostream>
#include "CItem.h"
#include <string>
#include <sstream>
#include <fstream>

void CItem::create_item(std::fstream &file, size_t &line_cnt)
{
    std::string line;
    size_t cnt=0;
    while(std::getline(file,line))
    {
        line_cnt++;
        if(cnt==0) //loads name of an item
        {
            if(change_format(line)=="")
            {
                std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
                throw std::runtime_error("The item has no name.");
            }

            m_name= line; //deletes spaces that are not neccesery

        }
        if(cnt==1) //loads description of an item
            m_description=line;
        if(cnt==2) //loads plus health of an item and checks it whether it is higher then 0
        {
            int tmp= check_int_item(line_cnt,line);
            if(tmp<=0)
            {
                std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
                throw std::runtime_error("The item hp stat is not set correctly");
            }
            m_health=tmp;
        }
        if(cnt==3) { //loads dmg of an item
            m_dmg= check_int_item(line_cnt,line);
        }
        if(cnt==4) { //loads shield of an item
            m_shield= check_int_item(line_cnt,line);
            break;
        }

        cnt++;
    }
    if(cnt<4) //if there are not enough arguments
    {
        std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
        throw std::runtime_error("The item doesn't have enough arguments");
    }
}

void CItem::print_item() const
{
    std::cout<<"Item: "<<m_name<<"-"<<m_description<<" "
            <<" | Health: "<<m_health<<" | Damage: "<<m_dmg<<
            " | Shield: "<<m_shield<<std::endl;}
int CItem::check_int_item(size_t line_cnt, std::string &line)const //checks string if there is not an invalid number
{
    int tmp=0;
    if(!check_int(line,tmp))
    {
        std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
        throw std::runtime_error("The item stat is not set correctly");
    }
    return tmp;
}