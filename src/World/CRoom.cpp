
#include "CRoom.h"
#include <set>
#include <string>
#include <fstream>
#include <iostream>
void CRoom::create_body(std::fstream &file, size_t &line_cnt)
{
    std::string line;
    size_t cnt = 0;
    while (std::getline(file, line))
    {
        line_cnt++;
        if (cnt==0) //loads id of a room
        {
            int tmp = check_int_room(line_cnt,line);
            m_id=tmp;
        }

    if(cnt==1) //loads a name of a room
        m_show=line;
    if(cnt==2) //loads a description of a room
        m_text=line;
    if(cnt==3) //loads dialogs that will be inside the room
    {
       if(!read_dialogs(line)) //checks whether the id is in good format and whether dialog does not alredy exists in a room
       {

           std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
           throw std::runtime_error("The dialog id is not set correctly.");
       }
       break;
    }

        cnt++;
    }
    if(cnt<3) //if the file is too short and some parameters are missing
    {
        std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
        throw std::runtime_error("Number of arguments in dialog is wrong.");
    }
}

int CRoom::check_int_room(size_t line_cnt, std::string &line)const //checks whether a room id is set correctly
{
    int tmp=0;
    if(!check_int(line,tmp))
    {
        std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
        throw std::runtime_error("The number in the dialog is not set correctly");
    }
    return tmp;
}

void CRoom::print_body() const
{
    std::cout<<"ROOM: "<<std::endl<<"ID: "<<m_id<<std::endl<<"SHOW: "<<m_show<<std::endl
             <<"TEXT: "<<m_text<<std::endl<<std::endl;
    for(auto itr: m_dialogs){
        std::cout<<itr<<" ";
    }
    std::cout<<std::endl;
}

bool CRoom::read_dialogs(std::string line)
{
    std::istringstream iss1(line);
    std::string tmp1;
    int tmp2=0;
    while (iss1 >> tmp1) { //reads dialogs ids from a line
        std::istringstream iss2(tmp1);
        if(!(iss2>>tmp2))
            return false;
        auto it=m_dialogs.find(tmp2);
        if(it!=m_dialogs.end())
            return false;
        m_dialogs.insert(tmp2);
    }
    return true;

}
