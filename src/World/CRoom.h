#pragma once
#include "CBody.h"
#include <set>
#include <sstream>
#include <algorithm>
#include "../utils.h"
/**
 * The class represents a room that consists of dialog
 */
class CRoom: public CBody
{
public:
    /**
     * Getter for ids of dialogs that are in a room
     * @return set with ids of dialogs
     */
    const std::set <int>& get_dialogs()const{
        return m_dialogs;
    }
    /**
     * Virtual method that creates either CRoom based on a data from configuration file
     * @param[in] file represents fstream with configuration file
     * @param[in] line_cnt represents number of lines in a configuration file
     */
    void create_body(std::fstream &file, size_t &line_cnt) override;
    /**
    * Check whether there is a num in a string
    * @param[in] line_cnt number of lines
    * @param [in] line string that will be checked
    * @return number that is in a string or it throws an excepiton
    */
    int check_int_room(size_t line_cnt, std::string &line)const;
    /**
     * The method will read dialogs from a room and check whether the input is vaid
     * @param[in] line represents a string with ids of dialogs
     * @return true if it was succesfull
     */
    bool read_dialogs(std::string line);
    /**
     *  Prints room on stdout
     */
    virtual void print_body()const override;


protected:
    /** represents dialogs inside a room*/
    std::set <int> m_dialogs;

};


