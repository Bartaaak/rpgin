#pragma once

#include <sstream>
#include <algorithm>
#include "../utils.h"
#include "CBody.h"
/**
 * The class represents an item that can be picked up by hero
 */
class CItem {
public:
    /**
     * Getter that returns name of an item
     * @return name of an item
     */
    std::string get_name ()const{
        return m_name;
    }
    /**
     * Getter for number of health
     * @return health
     */
    int get_health()const{
        return m_health;
    }
    /**
     * Getter for dmg
     * @return dmg
     */
    int get_dmg()const{
        return m_dmg;
    }
    /**
     * Getter for shield
     * @return shield
     */
    int get_shield()const{
        return m_shield;
    }
    /**
     * Getter for name of an item
     * @return name of an item
     */
    std::string & get_name(){
        return m_name;
    }
    /**
     * Sets CWorldPlay as friend class so that CWorldPlay can access protected members of CItem
     */
    friend class CWorldPlay;
    friend class CWorldPrepare;
    /**
     * Default constructor
     */
    CItem()=default;
    /**
     * This method will construct an item based on a configuration file
     * @param[in] file fstream that represents configuration file
     * @param[in] line_cnt number of lines in a configuration file
     */
    void create_item (std::fstream &file, size_t &line_cnt) ;
    /**
     *  prints an item on stdout
     */
    void print_item()const;
    /**
     * bool operator that compares 2 items based on their name
     * @param[in] other the other item
     * @return true if name of a current object is lexicographically larger then name of another object
     */
    bool operator>(const CItem& other) const {
        return m_name > other.m_name;
    }

    /**
     *  Check whether there is a num in a string
     * @param[in] line_cnt number of lines
     * @param[in] line string that will be checked
     * @return number that is in a string or it throws an excepiton
     */
    int check_int_item(size_t line_cnt, std::string &line)const;
    /**
     * Operator for copying from other CItem
     * @param[in] other the object that we are copying from
     * @return reference to current instance of an object
     */
    CItem& operator=(const CItem& other);


protected:
    /** name of an item*/
    std::string m_name;
    /** description of an item*/
    std::string m_description;
    /** healthpoint that will be added*/
    int m_health=0;
    /**damage that will be added*/
    int m_dmg=0;
    /**shield that will be added*/
    int m_shield=0;
};

