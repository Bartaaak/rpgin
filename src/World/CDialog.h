#pragma once
#include "CBody.h"
#include "../utils.h"
#include <string>
#include <algorithm>
#include  <iostream>
#include <fstream>
#include <sstream>
#define PICK_UP "PU" // Keyword used for pick up item dialog
#define COMBAT  "CB" // Keyword used for combat with an enemy dialog
#define NEXT_ROOM "NR" // Keyword used for going to a next room
#define PLAIN      "PL" //key word used for  a dialog that only show text
/**
 * The room consists of several dialogs, dialogs can have different types and they are represented by an id
 */
class CDialog: public CBody
{
public:
    /**
     * Creates dialog based on a config file
     * @param[in] file represents a fstream with a config file
     * @param[in] line_cnt represents number of current line
     */
    void create_body(std::fstream &file, size_t &line_cnt) override;
    /**
     * Checks an id in dialog whether it is valid
     * @param[in] line_cnt numer of lines
     * @param[in] line string to check from
     * @return the number in a line
     */
    int check_int_dialog(size_t line_cnt, std::string &line)const;
    /**
     * Prints dialogs
     */
    virtual void print_body()const override;
    /**
     * Declares CWorldPrepare and CWorldPlay as friend class so that they can access the protected members
     */
    friend class CWorldPrepare;
    friend class CWorldPlay;
    /**
     * Getter for name of an item or an enemy
     * @return name of an item or an enemy
     */
    std::string  get_action()const{
        return m_action;
    }
    /**
     * Getter for is_combat
     * @return true if it is a combat dialog against enemy, otherwise false
     */
    bool is_combat()const{
        if(m_type==COMBAT)
            return true;
        return false;
    }
    /**
     * Getter for is_pick_up
     * @return true if it is a dialog from which you can pick up item
     */
    bool is_pick_up()const{
        if(m_type==PICK_UP)
            return true;
        return false;
    }
    /**
     * Getter for is_next_room
     * @return true if a dialog moves you to a next room
     */
    bool is_next_room()const{
        if(m_type==NEXT_ROOM)
            return true;
        return false;
    }
    /**
     * Getter for is_plain dialog
     * @return true if it is a plain dialog that only shows a message
     */
    bool is_plain()const{
        if(m_type==PLAIN)
            return true;
        return false;
    }
    /**
     * Returns id of a dialog
     * @return id of an dialog
     */
    int get_id()const{
        return m_id;
    }





protected:
    /** represents type of a dialog*/
    std::string m_type;
    /** represents name of an entity that will do an action, for example item is grabbable or you can fight with an enemys */
    std::string m_action;


};

