
#include "CDialog.h"
#define PICK_UP "PU" // Keyword used for pick up item dialog
#define COMBAT  "CB" // Keyword used for combat with an enemy dialog
#define NEXT_ROOM "NR" // Keyword used for going to a next room
#define PLAIN      "PL" //key word used for  a dialog that only show text

void CDialog::create_body(std::fstream &file, size_t &line_cnt) {
    std::string line;
    size_t cnt = 0;
    while (std::getline(file, line))
    {
        line_cnt++;
        if (cnt==0) // loads id from configuration file
        {
            int tmp = check_int_dialog(line_cnt,line);
            m_id=tmp;
        }
        if(cnt==1) //loads a text that will be shown.
            m_show=line;
        if(cnt==2)// loads a text taht will be shown after an interaction
            m_text=line;
        if(cnt==3) //loads type of dialog
        {
            change_format(line);
            if(line!=PICK_UP && line!=COMBAT && line!=NEXT_ROOM && line!= PLAIN)
            {
                std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
                throw std::runtime_error("The type of dialog is not valid.");
            }
            m_type=line;
            if(line==NEXT_ROOM || line==PLAIN) //if it is next room or plain then break, if it is not, you need to specify the item or the enemy
                break;
        }
        if(cnt==4) //loads name of an entity, if it is plain or next room, this is not called
        {

            change_format(line);
            m_action=line;
            cnt--; //so that it works with a condition underneath
            break;
        }

            cnt++;
    }
    if (cnt < 3) { //if there are not enough arguments
        std::cout << "Config:A problem near line " << line_cnt << std::endl;
        throw std::runtime_error("The dialog doesn't have enough arguments");
    }
}
    int CDialog::check_int_dialog(size_t line_cnt, std::string &line)const //check whether an id is set correctly
    {
        int tmp=0;
        if(!check_int(line,tmp))
        {
            std::cout<<"Config:A problem near line "<< line_cnt<<std::endl;
            throw std::runtime_error("The number in a dialog is not set correctly");
        }
        return tmp;
    }

void CDialog::print_body() const
{
    std::cout<<"DIALOG: "<<std::endl<<"ID: "<<m_id<<std::endl<<"SHOW: "<<m_show<<std::endl
    <<"TEXT: "<<m_text<<std::endl<<"TYPE: "<<m_type<<std::endl;
}

