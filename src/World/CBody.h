#pragma once

#include <string>
/**
 * The class represents a body that can be either dialog or room
 */
class CBody {
public:
    /**
     *  Default constructor
     */
    CBody()=default;

    /**
     * Getter that returns id.
     * @return
     */
     const std::string get_show()const{
        return m_show;
     }
     /**
      * Getter that returns text
      * @return text
      */
     const std::string get_text()const{
         return m_text;
     }
     /**
      * Getter for m_id
      * @return id of a body
      */
     int get_id()const{
         return m_id;
     }

     /**
      *Declares CWorldPlay as friend class so that it can access protected members
      */
     friend class CWorldPlay;
     /**
     * Default destructor
     */
     virtual ~CBody()=default;
     /**
      * Virtual method that creates either CDialog or CRoom based on a data from configuration file
      * @param[in] file represents fstream with configuration file
      * @param[in] line_cnt represents number of lines in a configuration file
      */
     virtual void create_body(std::fstream & file, size_t & line_cnt )=0;
     /**
      * Virtual method that prints on a standart output
      */
     virtual void print_body()const=0;

protected:
     /** Represents a text that will be shown to a user*/
     std::string m_show;
     /** Represents a text that will be shown to a user after interaction*/
     std::string m_text;
     /** Represents an id of an user*/
     int m_id;


};

