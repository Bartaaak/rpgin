CXX=g++
LD=g++
CXX_FLAGS=-Wall -pedantic -Wextra -g
OUTPUT_DIR=out
EXECUTABLE=../bartami8

# List of source files
SOURCES := \
    src/utils.cpp \
    src/World/CBody.cpp \
    src/main.cpp \
    src/Character/CAbility.cpp \
    src/Character/CAbilityHp.cpp \
    src/Character/CAbilityDmg.cpp \
    src/Character/CAbilitySh.cpp \
    src/Character/CCombat.cpp \
    src/Character/CEnemy.cpp \
    src/Character/CHero.cpp \
    src/Character/CInventory.cpp \
    src/Config/CMenu.cpp \
    src/Config/CSLGame.cpp \
    src/Config/CWorldCheck.cpp \
    src/Config/CWorldPlay.cpp \
    src/Config/CWorldPrepare.cpp \
    src/World/CDialog.cpp \
    src/World/CItem.cpp \
    src/Config/CConfig.cpp \
    src/World/CRoom.cpp

# Generate a list of object files by replacing the .cpp extension with .o
OBJECTS := $(patsubst src/%.cpp,$(OUTPUT_DIR)/%.o,$(SOURCES))

# Default target: depends on the executable file specified by $(OUTPUT_DIR)/$(EXECUTABLE) and will also execute the doxygen command.
all: $(OUTPUT_DIR)/$(EXECUTABLE)
	doxygen Doxyfile

# This target specifies how to link the object files into the final executable.
$(OUTPUT_DIR)/$(EXECUTABLE): $(OBJECTS)
	$(LD) $(CXX_FLAGS) -o $@ $^

# This target specifies how to compile each individual source file into an object file.
$(OUTPUT_DIR)/%.o: src/%.cpp
	mkdir -p $(dir $@)
	$(CXX) $(CXX_FLAGS) -c -o $@ $<

clean:
	-rm -rf $(OUTPUT_DIR) doc
	-rm -rf bartami8

doc:
	doxygen Doxyfile

run:
	./$(OUTPUT_DIR)/$(EXECUTABLE)

deps:
	$(CXX) -MM $(SOURCES) > Makefile.d

compile: $(OUTPUT_DIR)/$(EXECUTABLE)

valrun:
	valgrind ./$(OUTPUT_DIR)/$(EXECUTABLE)

-include Makefile.d
# $@: Represents the target of the current rule being executed. Name of file being generated/updated
# $^: Represents the list of all dependencies of the current rule. 
# $<: Represents the first dependency of the current rule.
# -c: Tells the compiler to perform compilation only and not linking.
# -o $@: Specifies the output file name, which is set to the target name ($@) of the rule being executed.
